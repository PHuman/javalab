package com.epam.news.domain;

import java.sql.Timestamp;
import java.util.Date;

public class Comment extends Entity {
    private String text;
    private Timestamp creationDate;

    public Comment() {
    }

    public Comment(long id, String text, Timestamp creationDate) {
        super(id);
        this.text = text;
        this.creationDate = creationDate;
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

    public Timestamp getCreationDate() {
        return creationDate;
    }

    public void setCreationDate(Timestamp creationDate) {
        this.creationDate = creationDate;
    }

}
