package com.epam.news.domain;

import java.io.Serializable;
import java.util.List;

public class SearchCriteria implements Serializable {
    private Author author;
    private List<Tag> tags;

    public SearchCriteria() {
    }

    public SearchCriteria(Author author, List<Tag> tags) {
        this.author = author;
        this.tags = tags;
    }

    public Author getAuthor() {
        return author;
    }

    public void setAuthor(Author author) {
        this.author = author;
    }

    public List<Tag> getTags() {
        return tags;
    }

    public void setTags(List<Tag> tags) {
        this.tags = tags;
    }
}
