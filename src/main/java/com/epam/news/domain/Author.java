package com.epam.news.domain;


import java.sql.Timestamp;

public class Author extends Entity {
    private String name;
    private Timestamp expired;

    public Author() {
    }

    public Author(long id, String name) {
        super(id);
        this.name = name;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Timestamp getExpired() {
        return expired;
    }

    public void setExpired(Timestamp expired) {
        this.expired = expired;
    }

}
