package com.epam.news.dao;

import com.epam.news.domain.Comment;
import com.epam.news.exception.DaoException;

import java.util.List;

public interface CommentDao {
    /**
     * save Comment and return id
     *
     * @param comment
     * @throws DaoException
     */
    long save(Comment comment) throws DaoException;

    /**
     * delete comments by News id
     *
     * @param newsId
     * @throws DaoException
     */
    void deleteByNewsId(long newsId) throws DaoException;

    /**
     * delete comment by Comments id
     *
     * @param commentsId
     * @throws DaoException
     */
    void deleteById(long commentsId) throws DaoException;

    /**
     * load all Comments for current News by News id
     *
     * @param newsId
     * @throws DaoException
     */
    List<Comment> load(long newsId) throws DaoException;
}
