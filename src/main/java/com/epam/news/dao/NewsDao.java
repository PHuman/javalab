package com.epam.news.dao;

import com.epam.news.domain.Author;
import com.epam.news.domain.News;
import com.epam.news.domain.SearchCriteria;
import com.epam.news.domain.Tag;
import com.epam.news.exception.DaoException;

import java.util.List;

public interface NewsDao {
    /**
     * save News and return id
     *
     * @param news
     * @return id
     * @throws DaoException
     */
    Long save(News news) throws DaoException;

    /**
     * edit News, News must have id
     *
     * @param news
     * @throws DaoException
     */
    void edit(News news) throws DaoException;

    /**
     * delete News by id
     *
     * @param id
     * @throws DaoException
     */
    void delete(Long id) throws DaoException;

    /**
     * load all News
     *
     * @return List<News>
     * @throws DaoException
     */
    List<News> loadAllNews() throws DaoException;

    /**
     * load News by id
     *
     * @param newsId
     * @return News
     * @throws DaoException
     */
    News load(Long newsId) throws DaoException;

    /**
     * save link to Author from News
     *
     * @param authorId
     * @param newsId
     * @throws DaoException
     */
    void saveLinkToAuthor(Long authorId, Long newsId) throws DaoException;

    /**
     * delete link to Author from News
     *
     * @param newsId
     * @throws DaoException
     */
    void deleteLinkToAuthor(Long newsId) throws DaoException;

    /**
     * save link to one or several Tags from News
     *
     * @param tagsId
     * @param newsId
     * @throws DaoException
     */
    void saveLinkToTags(List<Long> tagsId, Long newsId) throws DaoException;

    /**
     * delete link to all Tags from News
     *
     * @param newsId
     * @throws DaoException
     */
    void deleteLinkToTags(Long newsId) throws DaoException;

    /**
     * return quantity of News
     *
     * @return News quantity
     * @throws DaoException
     */
    Long countNews() throws DaoException;

    /**
     * find all news by criteria (author and tags), tags or author can be null
     *
     * @param criteria
     * @return News
     * @throws DaoException
     */
    List<News> findNews(SearchCriteria criteria) throws DaoException;
}
