package com.epam.news.dao.impl;

import com.epam.news.dao.NewsDao;
import com.epam.news.domain.News;
import com.epam.news.domain.SearchCriteria;
import com.epam.news.domain.Tag;
import com.epam.news.exception.DaoException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.datasource.DataSourceUtils;

import javax.sql.DataSource;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

public class NewsDaoImpl implements NewsDao {
    @Autowired
    private DataSource source;
    private static final String SQL_SAVE = "INSERT INTO NEWS (NWS_TITLE, NWS_SHORT_TEXT," +
            " NWS_FULL_TEXT, NWS_CREATION_DATE, NWS_MODIFICATION_DATE) VALUES (?,?,?,?,?)";
    private static final String SQL_UPDATE = "UPDATE NEWS SET NWS_TITLE = ?, NWS_SHORT_TEXT = ?," +
            " NWS_FULL_TEXT = ?, NWS_MODIFICATION_DATE= ? WHERE NWS_NEWS_ID = ?";
    private static final String SQL_LOAD = "SELECT  NWS_TITLE, NWS_SHORT_TEXT, NWS_FULL_TEXT," +
            " NWS_CREATION_DATE, NWS_MODIFICATION_DATE FROM NEWS WHERE NWS_NEWS_ID = ?";
    private static final String SQL_LOAD_ALL_SORTED = "SELECT NWS_NEWS_ID, NWS_TITLE, NWS_SHORT_TEXT, NWS_FULL_TEXT," +
            " NWS_CREATION_DATE, NWS_MODIFICATION_DATE, (SELECT COUNT(*) FROM COMMENTS WHERE COM_NEWS_ID = NWS_NEWS_ID)" +
            " AS COMMENT_COUNT FROM NEWS ORDER BY NWS_MODIFICATION_DATE, COMMENT_COUNT DESC";
    private static final String SQL_DELETE = "DELETE FROM NEWS WHERE NWS_NEWS_ID = ?";
    private static final String SQL_LINK_TO_AUTHOR = "INSERT INTO NEWS_AUTHORS (NAUT_NEWS_ID, NAUT_AUTHOR_ID) VALUES (?,?)";
    private static final String SQL_DELETE_LINK_TO_AUTHOR = "DELETE FROM NEWS_AUTHORS WHERE NWS_NEWS_ID = ?";
    private static final String SQL_LINK_TO_TAGS = "INSERT INTO NEWS_TAGS (NTAG_NEWS_ID, NTAG_TAG_ID) VALUES (?,?)";
    private static final String SQL_DELETE_LINK_TO_TAGS = "DELETE FROM NEWS_TAGS WHERE NWS_NEWS_ID = ?";
    private static final String SQL_COUNT = "SELECT COUNT(*) FROM NEWS";
    private static final String SQL_FIND_BY_CRITERIA = "SELECT DISTINCT NWS_NEWS_ID, NWS_TITLE, NWS_SHORT_TEXT, NWS_FULL_TEXT," +
            " NWS_CREATION_DATE, NWS_MODIFICATION_DATE" +
            " FROM NEWS INNER JOIN NEWS_TAGS ON  NWS_NEWS_ID = NTAG_NEWS_ID " +
            "INNER JOIN NEWS_AUTHORS ON NWS_NEWS_ID = NAUT_NEWS_ID WHERE ";
    private static final String ADD_AUTHOR = "AUT_AUTHOR_ID = ?";
    private static final String ADD_TAGS = "NTAG_TAG_ID in(";

    /**
     * @see NewsDao#save(News)
     */
    @Override
    public Long save(News news) throws DaoException {
        Optional<Long> newsId;
        Connection connection = DataSourceUtils.getConnection(source);
        try (PreparedStatement preparedStatement = connection.prepareStatement(SQL_SAVE, new int[]{1})) {
            preparedStatement.setString(1, news.getTitle());
            preparedStatement.setString(2, news.getShortText());
            preparedStatement.setString(3, news.getFullText());
            preparedStatement.setTimestamp(4, news.getCreationDate());
            preparedStatement.setDate(5, news.getModificationDate());
            preparedStatement.execute();
            ResultSet rs = preparedStatement.getGeneratedKeys();
            rs.next();
            newsId = Optional.of(rs.getLong(1));
        } catch (SQLException e) {
            throw new DaoException("Exception when News title = " + news.getTitle(), e);
        } finally {
            DataSourceUtils.releaseConnection(connection, source);
        }
        return newsId.orElseThrow(() -> new DaoException("Exception when News title = " + news.getTitle()));
    }

    /**
     * @see NewsDao#edit(News)
     */
    @Override
    public void edit(News news) throws DaoException {
        Connection connection = DataSourceUtils.getConnection(source);
        try (PreparedStatement preparedStatement = connection.prepareStatement(SQL_UPDATE)) {
            preparedStatement.setString(1, news.getTitle());
            preparedStatement.setString(2, news.getShortText());
            preparedStatement.setString(3, news.getFullText());
            preparedStatement.setDate(4, news.getModificationDate());
            preparedStatement.setLong(5, news.getId());
            preparedStatement.execute();
        } catch (SQLException e) {
            throw new DaoException("Exception when News title = " + news.getTitle(), e);
        } finally {
            DataSourceUtils.releaseConnection(connection, source);
        }
    }

    /**
     * @see NewsDao#delete(Long)
     */
    @Override
    public void delete(Long id) throws DaoException {
        Connection connection = DataSourceUtils.getConnection(source);
        try (PreparedStatement preparedStatement = connection.prepareStatement(SQL_DELETE)) {
            preparedStatement.setLong(1, id);
            preparedStatement.execute();
        } catch (SQLException e) {
            throw new DaoException("Exception when id = " + id, e);
        } finally {
            DataSourceUtils.releaseConnection(connection, source);
        }
    }

    /**
     * @see NewsDao#loadAllNews()
     */
    @Override
    public List<News> loadAllNews() throws DaoException {
        List<News> news = new ArrayList<>();
        Connection connection = DataSourceUtils.getConnection(source);
        try (PreparedStatement preparedStatement = connection.prepareStatement(SQL_LOAD_ALL_SORTED)) {
            ResultSet result = preparedStatement.executeQuery();
            while (result.next()) {
                News currentNews = new News(result.getLong(1), result.getString(2), result.getString(3),
                        result.getString(4), result.getTimestamp(5), result.getDate(6));
                news.add(currentNews);
            }
        } catch (SQLException e) {
            throw new DaoException(e);
        } finally {
            DataSourceUtils.releaseConnection(connection, source);
        }
        return news;
    }

    /**
     * @see NewsDao#load(Long)
     */
    @Override
    public News load(Long newsId) throws DaoException {
        News news = null;
        Connection connection = DataSourceUtils.getConnection(source);
        try (PreparedStatement preparedStatement = connection.prepareStatement(SQL_LOAD)) {
            preparedStatement.setLong(1, newsId);
            ResultSet result = preparedStatement.executeQuery();
            result.next();
            news = new News(result.getInt(1), result.getString(2), result.getString(3),
                    result.getString(4), result.getTimestamp(5), result.getDate(6));
        } catch (SQLException e) {
            throw new DaoException("Exception when id = " + newsId, e);
        } finally {
            DataSourceUtils.releaseConnection(connection, source);
        }
        return news;
    }

    /**
     * @see NewsDao#saveLinkToAuthor(Long, Long)
     */
    @Override
    public void saveLinkToAuthor(Long authorId, Long newsId) throws DaoException {
        Connection connection = DataSourceUtils.getConnection(source);
        try (PreparedStatement preparedStatement = connection.prepareStatement(SQL_LINK_TO_AUTHOR)) {
            preparedStatement.setLong(1, newsId);
            preparedStatement.setLong(2, authorId);
            preparedStatement.execute();
        } catch (SQLException e) {
            throw new DaoException("Exception when Author id = " + authorId + " News id = " + newsId, e);
        } finally {
            DataSourceUtils.releaseConnection(connection, source);
        }
    }

    /**
     * @see NewsDao#deleteLinkToAuthor(Long)
     */
    @Override
    public void deleteLinkToAuthor(Long newsId) throws DaoException {
        Connection connection = DataSourceUtils.getConnection(source);
        try (PreparedStatement preparedStatement = connection.prepareStatement(SQL_DELETE_LINK_TO_AUTHOR)) {
            preparedStatement.setLong(1, newsId);
            preparedStatement.execute();
        } catch (SQLException e) {
            throw new DaoException("Exception when News id = " + newsId, e);
        } finally {
            DataSourceUtils.releaseConnection(connection, source);
        }
    }

    /**
     * @see NewsDao#saveLinkToTags(List, Long)
     */
    @Override
    public void saveLinkToTags(List<Long> tagsId, Long newsId) throws DaoException {
        Connection connection = DataSourceUtils.getConnection(source);
        try (PreparedStatement preparedStatement = connection.prepareStatement(SQL_LINK_TO_TAGS)) {
            for (Long tagId : tagsId) {
                preparedStatement.setLong(1, newsId);
                preparedStatement.setLong(2, tagId);
                preparedStatement.addBatch();
            }
            preparedStatement.executeBatch();
        } catch (SQLException e) {
            throw new DaoException("Exception when newsId" + newsId, e);
        } finally {
            DataSourceUtils.releaseConnection(connection, source);
        }
    }

    /**
     * @see NewsDao#deleteLinkToTags(Long)
     */
    @Override
    public void deleteLinkToTags(Long newsId) throws DaoException {
        Connection connection = DataSourceUtils.getConnection(source);
        try (PreparedStatement preparedStatement = connection.prepareStatement(SQL_DELETE_LINK_TO_TAGS)) {
            preparedStatement.setLong(1, newsId);
            preparedStatement.execute();
        } catch (SQLException e) {
            throw new DaoException("Exception when News id = " + newsId, e);
        } finally {
            DataSourceUtils.releaseConnection(connection, source);
        }
    }

    /**
     * @see NewsDao#countNews()
     */
    @Override
    public Long countNews() throws DaoException {
        Optional<Long> count;
        Connection connection = DataSourceUtils.getConnection(source);
        try (PreparedStatement preparedStatement = connection.prepareStatement(SQL_COUNT)) {
            ResultSet result = preparedStatement.executeQuery();
            result.next();
            count = Optional.of(result.getLong(1));
        } catch (SQLException e) {
            throw new DaoException(e);
        } finally {
            DataSourceUtils.releaseConnection(connection, source);
        }
        return count.orElseThrow(DaoException::new);
    }

    /**
     * @see NewsDao#findNews(SearchCriteria)
     */
    @Override
    public List<News> findNews(SearchCriteria criteria) throws DaoException {
        List<News> news = new ArrayList<>();
        boolean flag = false;
        StringBuilder query = new StringBuilder(SQL_FIND_BY_CRITERIA);
        if (criteria.getAuthor() != null) {
            query.append(ADD_AUTHOR);
            flag = true;
        }
        if (criteria.getTags() != null && !criteria.getTags().isEmpty()) {
            if (flag) {
                query.append(" AND ");
            }
            query.append(ADD_TAGS);
            for (Tag tag : criteria.getTags()) {
                query.append(tag.getId()).append(",");
            }
            query.deleteCharAt(query.length() - 1);
            query.append(")");
        }
        Connection connection = DataSourceUtils.getConnection(source);
        try (PreparedStatement preparedStatement = connection.prepareStatement(query.toString())) {
            ResultSet result = preparedStatement.executeQuery();
            while (result.next()) {
                News currentNews = new News(result.getLong(1), result.getString(2), result.getString(3),
                        result.getString(4), result.getTimestamp(5), result.getDate(6));
                news.add(currentNews);
            }
        } catch (SQLException e) {
            throw new DaoException("Exception when Author name = " + criteria.getAuthor().getName(), e);
        } finally {
            DataSourceUtils.releaseConnection(connection, source);
        }
        return news;
    }
}
