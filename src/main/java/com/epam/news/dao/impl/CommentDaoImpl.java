package com.epam.news.dao.impl;

import com.epam.news.dao.CommentDao;
import com.epam.news.domain.Comment;
import com.epam.news.exception.DaoException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.datasource.DataSourceUtils;

import javax.sql.DataSource;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

public class CommentDaoImpl implements CommentDao {
    @Autowired
    private DataSource source;
    private static final String SQL_DELETE_BY_NEWS_ID = "DELETE FROM COMMENTS WHERE COM_NEWS_ID = ?";
    private static final String SQL_DELETE_BY_ID = "DELETE FROM COMMENTS WHERE COM_COMMENT_ID = ?";
    private static final String SQL_SAVE = "INSERT INTO COMMENTS (COM_NEWS_ID, COM_COMMENT_TEXT, COM_CREATION_DATE) VALUES (?,?,?)";
    private static final String SQL_LOAD = "SELECT COM_COMMENT_ID, COM_COMMENT_TEXT," +
            " COM_CREATION_DATE FROM COMMENTS WHERE COM_NEWS_ID = ?";

    /**
     * @see CommentDao#save(Comment)
     */
    @Override
    public long save(Comment comment) throws DaoException {
        Optional<Long> commentId;
        Connection connection = DataSourceUtils.getConnection(source);
        try (PreparedStatement preparedStatement = connection.prepareStatement(SQL_SAVE, new int[]{1})) {
            preparedStatement.setLong(1, comment.getId());
            preparedStatement.setString(2, comment.getText());
            preparedStatement.setTimestamp(3, comment.getCreationDate());
            preparedStatement.execute();
            ResultSet rs = preparedStatement.getGeneratedKeys();
            rs.next();
            commentId = Optional.of(rs.getLong(1));
        } catch (SQLException e) {
            throw new DaoException("Exception when Comment text = " + comment.getText(), e);
        } finally {
            DataSourceUtils.releaseConnection(connection, source);
        }
        return commentId.orElseThrow(() -> new DaoException("Exception when Comment text = " + comment.getText()));
    }

    /**
     * @see CommentDao#deleteByNewsId(long)
     */
    @Override
    public void deleteByNewsId(long newsId) throws DaoException {
        Connection connection = DataSourceUtils.getConnection(source);
        try (PreparedStatement preparedStatement = connection.prepareStatement(SQL_DELETE_BY_NEWS_ID)) {
            preparedStatement.setLong(1, newsId);
            preparedStatement.execute();
        } catch (SQLException e) {
            throw new DaoException("Exception when News id " + newsId, e);
        } finally {
            DataSourceUtils.releaseConnection(connection, source);
        }
    }

    /**
     * @see CommentDao#deleteById(long)
     */
    @Override
    public void deleteById(long commentsId) throws DaoException {
        Connection connection = DataSourceUtils.getConnection(source);
        try (PreparedStatement preparedStatement = connection.prepareStatement(SQL_DELETE_BY_ID)) {
            preparedStatement.setLong(1, commentsId);
            preparedStatement.execute();
        } catch (SQLException e) {
            throw new DaoException("Exception when Comment id " + commentsId, e);
        } finally {
            DataSourceUtils.releaseConnection(connection, source);
        }
    }

    /**
     * @see CommentDao#load(long)
     */
    @Override
    public List<Comment> load(long newsId) throws DaoException {
        List<Comment> comments = new ArrayList<>();
        Connection connection = DataSourceUtils.getConnection(source);
        try (PreparedStatement preparedStatement = connection.prepareStatement(SQL_LOAD)) {
            preparedStatement.setLong(1, newsId);
            ResultSet result = preparedStatement.executeQuery();
            while (result.next()) {
                Comment comment = new Comment(result.getLong(1), result.getString(2), result.getTimestamp(3));
                comments.add(comment);
            }
        } catch (SQLException e) {
            throw new DaoException("Exception when News id " + newsId, e);
        } finally {
            DataSourceUtils.releaseConnection(connection, source);
        }
        return comments;
    }
}
