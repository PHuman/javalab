package com.epam.news.dao.impl;

import com.epam.news.dao.AuthorDao;
import com.epam.news.domain.Author;
import com.epam.news.exception.DaoException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.datasource.DataSourceUtils;

import javax.sql.DataSource;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Optional;


public class AuthorDaoImpl implements AuthorDao {
    @Autowired
    private DataSource source;
    private static final String SQL_SAVE = "INSERT INTO AUTHORS (AUT_AUTHOR_NAME) VALUES (?)";
    private static final String SQL_UPDATE = "UPDATE AUTHORS SET AUT_EXPIRED=? WHERE AUT_AUTHOR_NAME = ?";
    private static final String SQL_LOAD_BY_NEWS = "SELECT AUT_AUTHOR_ID, AUT_AUTHOR_NAME,AUT_EXPIRED  FROM AUTHORS INNER JOIN" +
            " NEWS_AUTHORS ON NAUT_AUTHOR_ID = AUT_AUTHOR_ID WHERE NAUT_NEWS_ID = ?";
    private static final String SQL_LOAD_BY_NAME = "SELECT AUT_AUTHOR_ID, AUT_AUTHOR_NAME, " +
            "AUT_EXPIRED FROM AUTHORS WHERE AUT_AUTHOR_NAME = ?";

    /**
     * @see AuthorDao#save(Author)
     */
    @Override
    public long save(Author author) throws DaoException {
        Optional<Long> authorId;
        Connection connection = DataSourceUtils.getConnection(source);
        try (PreparedStatement preparedStatement = connection.prepareStatement(SQL_SAVE, new int[]{1})) {
            preparedStatement.setString(1, author.getName());
            preparedStatement.execute();
            ResultSet rs = preparedStatement.getGeneratedKeys();
            rs.next();
            authorId = Optional.of(rs.getLong(1));
        } catch (SQLException e) {
            throw new DaoException("Exception when Author name = " + author.getName(), e);
        } finally {
            DataSourceUtils.releaseConnection(connection, source);
        }
        return authorId.orElseThrow(() -> new DaoException("Exception when Author name = " + author.getName()));
    }

    /**
     * @see AuthorDao#update(Author)
     */
    @Override
    public void update(Author author) throws DaoException {
        Connection connection = DataSourceUtils.getConnection(source);
        try (PreparedStatement preparedStatement = connection.prepareStatement(SQL_UPDATE)) {
            preparedStatement.setTimestamp(1, author.getExpired());
            preparedStatement.setString(2, author.getName());
            preparedStatement.execute();
        } catch (SQLException e) {
            throw new DaoException("Exception when Author name = " + author.getName(), e);
        } finally {
            DataSourceUtils.releaseConnection(connection, source);
        }
    }

    /**
     * @see AuthorDao#loadByNews(long)
     */
    @Override
    public Author loadByNews(long newsId) throws DaoException {
        Optional<Author> author;
        Connection connection = DataSourceUtils.getConnection(source);
        try (PreparedStatement preparedStatement = connection.prepareStatement(SQL_LOAD_BY_NEWS)) {
            preparedStatement.setLong(1, newsId);
            ResultSet rs = preparedStatement.executeQuery();
            rs.next();
            author = Optional.of(new Author(rs.getLong(1), rs.getString(2)));
            author.get().setExpired(rs.getTimestamp(3));
        } catch (SQLException e) {
            throw new DaoException("Exception when News id = " + newsId, e);
        } finally {
            DataSourceUtils.releaseConnection(connection, source);
        }
        return author.orElseThrow(() -> new DaoException("Exception when News id = " + newsId));
    }

    /**
     * @see AuthorDao#loadByName(String)
     */
    @Override
    public Author loadByName(String authorName) throws DaoException {
        Author author;
        Connection connection = DataSourceUtils.getConnection(source);
        try (PreparedStatement preparedStatement = connection.prepareStatement(SQL_LOAD_BY_NAME)) {
            preparedStatement.setString(1, authorName);
            ResultSet rs = preparedStatement.executeQuery();
            rs.next();
            author = new Author(rs.getLong(1), rs.getString(2));
            author.setExpired(rs.getTimestamp(3));
        } catch (SQLException e) {
            throw new DaoException("Exception when Author name = " + authorName, e);
        } finally {
            DataSourceUtils.releaseConnection(connection, source);
        }
        return author;
    }
}
