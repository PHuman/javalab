package com.epam.news.dao;

import com.epam.news.domain.Tag;
import com.epam.news.exception.DaoException;

import java.util.List;

public interface TagDao {
    /**
     * save Tag and return id
     *
     * @param tag
     * @return id
     * @throws DaoException
     */
    Long save(Tag tag) throws DaoException;

    /**
     * load Tag by name
     *
     * @param tagName
     * @return Tag
     * @throws DaoException
     */
    Tag loadByName(String tagName) throws DaoException;

    /**
     * load all tags for current News by News id
     *
     * @param newsId
     * @return List<Tag>
     * @throws DaoException
     */
    List<Tag> load(Long newsId) throws DaoException;
}
