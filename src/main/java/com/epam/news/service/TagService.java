package com.epam.news.service;

import com.epam.news.domain.Tag;
import com.epam.news.exception.ServiceException;

import java.util.List;

public interface TagService {
    /**
     * save Tag and return true if operation is success
     *
     * @param tag
     * @return true if operation is success
     * @throws ServiceException
     */
    boolean save(Tag tag) throws ServiceException;

    /**
     * load Tags for current News by News id
     *
     * @param newsId
     * @return List<Tag>
     * @throws ServiceException
     */
    List<Tag> load(Long newsId) throws ServiceException;
}
