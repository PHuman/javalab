package com.epam.news.service.impl;

import com.epam.news.dao.TagDao;
import com.epam.news.domain.Tag;
import com.epam.news.exception.DaoException;
import com.epam.news.exception.ServiceException;
import com.epam.news.service.TagService;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.List;

public class TagServiceImpl implements TagService {
    @Autowired
    private TagDao tagDao;

    /**
     * @see TagService#save(Tag)
     */
    @Override
    public boolean save(Tag tag) throws ServiceException {
        boolean flag = false;
        try {
            if (tagDao.loadByName(tag.getName()) == null) {
                tagDao.save(tag);
                flag = true;
            }
        } catch (DaoException e) {
            throw new ServiceException(e);
        }
        return flag;
    }

    /**
     * @see TagService#load(Long)
     */
    @Override
    public List<Tag> load(Long newsId) throws ServiceException {
        try {
            return tagDao.load(newsId);
        } catch (DaoException e) {
            throw new ServiceException(e);
        }
    }
}
