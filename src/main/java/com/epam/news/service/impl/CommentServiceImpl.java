package com.epam.news.service.impl;

import com.epam.news.dao.CommentDao;
import com.epam.news.domain.Comment;
import com.epam.news.exception.DaoException;
import com.epam.news.exception.ServiceException;
import com.epam.news.service.CommentService;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.List;

public class CommentServiceImpl implements CommentService {
    @Autowired
    private CommentDao commentDao;

    /**
     * @see CommentService#save(Comment)
     */
    @Override
    public void save(Comment comment) throws ServiceException {
        try {
            commentDao.save(comment);
        } catch (DaoException e) {
            throw new ServiceException(e);
        }
    }

    /**
     * @see CommentService#delete(long)
     */
    @Override
    public void delete(long commentsId) throws ServiceException {
        try {
            commentDao.deleteById(commentsId);
        } catch (DaoException e) {
            throw new ServiceException(e);
        }
    }

    /**
     * @see CommentService#deleteByNewsId(Long)
     */
    @Override
    public void deleteByNewsId(Long newsId) throws ServiceException {
        try {
            commentDao.deleteByNewsId(newsId);
        } catch (DaoException e) {
            throw new ServiceException(e);
        }
    }

    /**
     * @see CommentService#loadByNewsId(Long)
     */
    @Override
    public List<Comment> loadByNewsId(Long newsId)throws ServiceException {
        try {
            return commentDao.load(newsId);
        } catch (DaoException e) {
            throw new ServiceException(e);
        }
    }

}
