package com.epam.news.service.impl;

import com.epam.news.domain.News;
import com.epam.news.domain.NewsTO;
import com.epam.news.exception.ServiceException;
import com.epam.news.service.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.List;

public class FacadeNewsServiceImpl implements FacadeNewsService {
    @Autowired
    private NewsService newsService;
    @Autowired
    private CommentService commentService;
    @Autowired
    private AuthorService authorService;
    @Autowired
    private TagService tagService;

    /**
     * @see FacadeNewsService#deleteNews(long)
     */
    @Transactional
    @Override
    public void deleteNews(long newsId) throws ServiceException {
        commentService.deleteByNewsId(newsId);
        newsService.deleteNews(newsId);
    }

    /**
     * @see FacadeNewsService#loadAllNews()
     */
    @Transactional
    @Override
    public List<NewsTO> loadAllNews() throws ServiceException {
        List<NewsTO> transferObjects = new ArrayList<>();
        for (News news : newsService.loadAllNews()) {
            NewsTO transferObject = new NewsTO();
            transferObject.setNews(news);
            transferObject.setAuthor(authorService.loadByNews(news.getId()));
            transferObject.setTags(tagService.load(news.getId()));
            transferObject.setComments(commentService.loadByNewsId(news.getId()));
            transferObjects.add(transferObject);
        }
        return transferObjects;
    }
}
