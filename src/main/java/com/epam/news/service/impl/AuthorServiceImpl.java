package com.epam.news.service.impl;

import com.epam.news.dao.AuthorDao;
import com.epam.news.domain.Author;
import com.epam.news.exception.DaoException;
import com.epam.news.exception.ServiceException;
import com.epam.news.service.AuthorService;
import org.springframework.beans.factory.annotation.Autowired;

public class AuthorServiceImpl implements AuthorService {
    @Autowired
    private AuthorDao authorDao;

    /**
     * @see AuthorService#save(Author)
     */
    @Override
    public boolean save(Author author) throws ServiceException {
        boolean flag = false;
        try {
            if (authorDao.loadByName(author.getName()) == null) {
                authorDao.save(author);
                flag = true;
            }
        } catch (DaoException e) {
            throw new ServiceException(e);
        }
        return flag;
    }

    /**
     * @see AuthorService#loadByNews(Long)
     */
    @Override
    public Author loadByNews(Long newsId) throws ServiceException {
        try {
            return authorDao.loadByNews(newsId);
        } catch (DaoException e) {
            throw new ServiceException(e);
        }
    }
}
