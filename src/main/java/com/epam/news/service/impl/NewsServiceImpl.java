package com.epam.news.service.impl;

import com.epam.news.dao.NewsDao;
import com.epam.news.domain.Entity;
import com.epam.news.domain.News;
import com.epam.news.domain.NewsTO;
import com.epam.news.domain.SearchCriteria;
import com.epam.news.exception.DaoException;
import com.epam.news.exception.ServiceException;
import com.epam.news.service.NewsService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.stream.Collectors;

public class NewsServiceImpl implements NewsService {
    @Autowired
    private NewsDao newsDao;

    /**
     * @see NewsService#saveNews(NewsTO)
     */
    @Transactional
    @Override
    public void saveNews(NewsTO transferObject) throws ServiceException {
        long newsId;
        try {
            newsId = newsDao.save(transferObject.getNews());
            newsDao.saveLinkToAuthor(transferObject.getAuthor().getId(), newsId);
            newsDao.saveLinkToTags(transferObject.getTags().stream().map(Entity::getId).collect(Collectors.toList()), newsId);
        } catch (DaoException e) {
            throw new ServiceException(e);
        }
    }

    /**
     * @see NewsService#editNews(News)
     */
    @Override
    public void editNews(News news) throws ServiceException {
        try {
            newsDao.edit(news);
        } catch (DaoException e) {
            throw new ServiceException(e);
        }
    }

    /**
     * @see NewsService#deleteNews(long)
     */
    @Override
    public void deleteNews(long newsId) throws ServiceException {
        try {
            newsDao.deleteLinkToAuthor(newsId);
            newsDao.deleteLinkToTags(newsId);
            newsDao.delete(newsId);
        } catch (DaoException e) {
            throw new ServiceException(e);
        }
    }

    /**
     * @see NewsService#countNews()
     */
    @Override
    public long countNews() throws ServiceException {
        try {
            return newsDao.countNews();
        } catch (DaoException e) {
            throw new ServiceException(e);
        }
    }

    /**
     * @see NewsService#loadNews(long)
     */
    @Override
    public News loadNews(long newsId) throws ServiceException {
        try {
            return newsDao.load(newsId);
        } catch (DaoException e) {
            throw new ServiceException(e);
        }
    }

    /**
     * @see NewsService#loadAllNews()
     */
    @Override
    public List<News> loadAllNews() throws ServiceException {
        try {
            return newsDao.loadAllNews();
        } catch (DaoException e) {
            throw new ServiceException(e);
        }
    }

    /**
     * @see NewsService#findNews(SearchCriteria)
     */
    @Override
    public List<News> findNews(SearchCriteria criteria) throws ServiceException {
        try {
            return newsDao.findNews(criteria);
        } catch (DaoException e) {
            throw new ServiceException(e);
        }
    }
}
