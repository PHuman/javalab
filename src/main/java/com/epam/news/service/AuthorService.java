package com.epam.news.service;

import com.epam.news.domain.Author;
import com.epam.news.exception.ServiceException;

public interface AuthorService {
    /**
     * save Author and return true if operation is success
     *
     * @param author
     * @return true if operation is success
     * @throws ServiceException
     */
    boolean save(Author author) throws ServiceException;

    /**
     * load Author by News id
     *
     * @param newsId
     * @return author
     * @throws ServiceException
     */
    Author loadByNews(Long newsId)throws ServiceException;
}
