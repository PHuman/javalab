package com.epam.news.service;

import com.epam.news.domain.News;
import com.epam.news.domain.NewsTO;
import com.epam.news.domain.SearchCriteria;
import com.epam.news.exception.ServiceException;

import java.util.List;

public interface NewsService {
    /**
     * save News, Author and Tags
     *
     * @param transferObject
     * @throws ServiceException
     */
    void saveNews(NewsTO transferObject) throws ServiceException;

    /**
     * edit News, News must have newsId
     *
     * @param news
     * @throws ServiceException
     */
    void editNews(News news) throws ServiceException;

    /**
     * @return count of all news
     * @throws ServiceException
     */
    public long countNews() throws ServiceException;

    /**
     * load News by id
     *
     * @param newsId
     * @return
     * @throws ServiceException
     */
    News loadNews(long newsId) throws ServiceException;

    /**
     * delete News by id
     *
     * @param newsId
     * @throws ServiceException
     */
    void deleteNews(long newsId) throws ServiceException;

    /**
     * load all News sorted by modification date and by most commented news
     *
     * @return List<News>
     * @throws ServiceException
     */
    List<News> loadAllNews() throws ServiceException;

    /**
     * @param criteria
     * @return news by criteria (author and tags), author or tags can be null
     * @throws ServiceException
     */
    List<News> findNews(SearchCriteria criteria) throws ServiceException;
}
