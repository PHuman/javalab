package com.epam.news.service;

import com.epam.news.domain.Comment;
import com.epam.news.exception.ServiceException;

import java.util.List;

public interface CommentService {
    /**
     * save Comment
     *
     * @param comment
     * @throws ServiceException
     */
    void save(Comment comment) throws ServiceException;

    /**
     * delete Comment by id
     *
     * @param commentsId
     * @throws ServiceException
     */
    void delete(long commentsId) throws ServiceException;

    /**
     *
     * @param newsId
     * @throws ServiceException
     */
    void deleteByNewsId(Long newsId)throws ServiceException;

    /**
     * load Comments for current News by News id
     *
     * @param newsId
     * @return List<Comment>
     * @throws ServiceException
     */
    List<Comment> loadByNewsId(Long newsId)throws ServiceException;
}
