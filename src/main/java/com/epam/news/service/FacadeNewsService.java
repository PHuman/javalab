package com.epam.news.service;

import com.epam.news.domain.News;
import com.epam.news.domain.NewsTO;
import com.epam.news.domain.SearchCriteria;
import com.epam.news.exception.ServiceException;

import java.util.List;

public interface FacadeNewsService {
    /**
     * delete News by id
     *
     * @param newsId
     * @throws ServiceException
     */
    void deleteNews(long newsId) throws ServiceException;

    /**
     * @return all news with  author and tags sorted by modification date and by most commented news
     * @throws ServiceException
     */
    List<NewsTO> loadAllNews() throws ServiceException;

}
