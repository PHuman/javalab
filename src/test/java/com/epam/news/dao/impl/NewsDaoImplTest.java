package com.epam.news.dao.impl;

import com.epam.news.dao.NewsDao;
import com.epam.news.exception.DaoException;
import com.epam.news.domain.News;
import com.epam.news.domain.Tag;
import com.epam.news.domain.SearchCriteria;
import com.github.springtestdbunit.DbUnitTestExecutionListener;
import com.github.springtestdbunit.annotation.DatabaseOperation;
import com.github.springtestdbunit.annotation.DatabaseSetup;
import com.github.springtestdbunit.annotation.DatabaseTearDown;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.TestExecutionListeners;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.support.DependencyInjectionTestExecutionListener;

import java.sql.Date;
import java.sql.Timestamp;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

import static org.junit.Assert.assertEquals;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = "/test-context.xml")
@TestExecutionListeners({DependencyInjectionTestExecutionListener.class, DbUnitTestExecutionListener.class})
@DatabaseSetup("/news-data.xml")
@DatabaseTearDown(value = "/news-data.xml", type = DatabaseOperation.DELETE_ALL)
public class NewsDaoImplTest {

    @Autowired
    private NewsDao newsDao;

    @Test
    public void testSaveNews() throws DaoException {
        long countBefore = newsDao.countNews();
        News news = new News();
        news.setShortText("Short");
        news.setFullText("Full");
        news.setTitle("Title");
        news.setCreationDate(Timestamp.valueOf(LocalDateTime.now()));
        news.setModificationDate(Date.valueOf(LocalDate.now()));
        newsDao.save(news);
        long countAfter = newsDao.countNews();
        assertEquals(countBefore + 1, countAfter);
        assertEquals(countBefore, 3);
    }

    @Test
    public void testGetAllNews() throws DaoException {
        List<News> newsList = newsDao.loadAllNews();
        newsDao.save(new News("News", "short", "full", Timestamp.valueOf(LocalDateTime.now()),
                Date.valueOf(LocalDate.now())));
        List<News> newsAfterAdding = newsDao.loadAllNews();
        assertEquals(newsList.size() + 1, newsAfterAdding.size());
    }

    @Test
    public void testFindNews() throws DaoException {
        List<Tag> tags = new ArrayList<>();
        SearchCriteria criteria = new SearchCriteria();
        tags.add(new Tag(1, "good"));
        tags.add(new Tag(2, "better"));
        tags.add(new Tag(3, "best"));
        tags.add(new Tag(4, "bad"));
        criteria.setTags(tags);
        List<News> newsList = newsDao.findNews(criteria);
        assertEquals(2, newsList.size());
    }

}