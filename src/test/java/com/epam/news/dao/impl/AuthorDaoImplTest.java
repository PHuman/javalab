package com.epam.news.dao.impl;

import com.epam.news.dao.AuthorDao;
import com.epam.news.domain.Author;
import com.epam.news.exception.DaoException;
import com.github.springtestdbunit.DbUnitTestExecutionListener;
import com.github.springtestdbunit.annotation.DatabaseOperation;
import com.github.springtestdbunit.annotation.DatabaseSetup;
import com.github.springtestdbunit.annotation.DatabaseTearDown;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.TestExecutionListeners;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.support.DependencyInjectionTestExecutionListener;

import java.sql.Timestamp;
import java.time.LocalDateTime;

import static org.junit.Assert.assertEquals;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = "/test-context.xml")
@TestExecutionListeners({DependencyInjectionTestExecutionListener.class, DbUnitTestExecutionListener.class})
@DatabaseSetup("/author-data.xml")
@DatabaseTearDown(value = "/author-data.xml", type = DatabaseOperation.DELETE_ALL)
public class AuthorDaoImplTest {

    @Autowired
    private AuthorDao authorDao;

    @Test
    public void testSave() throws DaoException {
        Author author = new Author(5, "Gogol");
        long insert = authorDao.save(author);
        long gogol = authorDao.loadByName("Gogol").getId();
        assertEquals(insert, gogol);
    }

    @Test
    public void testUpdate() throws DaoException {
        Timestamp timestamp = Timestamp.valueOf(LocalDateTime.now());
        Author author = new Author(1, "Пушкин");
        author.setExpired(timestamp);
        authorDao.update(author);
        Timestamp gogol1 = authorDao.loadByName("Пушкин").getExpired();
        assertEquals(timestamp, gogol1);
    }

    @Test
    public void testLoadByNews() throws DaoException {
        Author author = authorDao.loadByNews(1L);
        assertEquals(author.getName(), "Пушкин");
    }

    @Test
    public void testLoadByName() throws DaoException {
        Author author = authorDao.loadByName("Пушкин");
        assertEquals(author.getId(), 1L);
    }

}
