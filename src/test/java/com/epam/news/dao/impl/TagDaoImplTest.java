package com.epam.news.dao.impl;

import com.epam.news.domain.Tag;
import com.epam.news.exception.DaoException;
import com.github.springtestdbunit.DbUnitTestExecutionListener;
import com.github.springtestdbunit.annotation.DatabaseOperation;
import com.github.springtestdbunit.annotation.DatabaseSetup;
import com.github.springtestdbunit.annotation.DatabaseTearDown;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.TestExecutionListeners;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.support.DependencyInjectionTestExecutionListener;

import java.util.List;

import static org.junit.Assert.assertEquals;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = "/test-context.xml")
@TestExecutionListeners({DependencyInjectionTestExecutionListener.class, DbUnitTestExecutionListener.class})
@DatabaseSetup("/tags-data.xml")
@DatabaseTearDown(value = "/tags-data.xml", type = DatabaseOperation.DELETE_ALL)
public class TagDaoImplTest {
    @Autowired
    private TagDaoImpl tagDao;

    @Test
    public void testLoadByName() throws DaoException {
        Tag tag = new Tag(1L, "TEst");
        long id = tagDao.save(tag);
        Tag tEst = tagDao.loadByName("TEst");
        assertEquals(tEst.getId(), id);
    }

    @Test
    public void testLoad() throws DaoException {
        List<Tag> load = tagDao.load(1L);
        assertEquals(load.get(0).getName(), "good");
    }
}
