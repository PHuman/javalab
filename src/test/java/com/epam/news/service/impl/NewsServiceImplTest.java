package com.epam.news.service.impl;

import com.epam.news.dao.NewsDao;
import com.epam.news.domain.*;
import com.epam.news.exception.DaoException;
import com.epam.news.exception.ServiceException;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import java.util.ArrayList;
import java.util.stream.Collectors;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = "classpath:/context.xml")
public class NewsServiceImplTest {
    @Mock
    private NewsDao newsDao;
    @InjectMocks
    private NewsServiceImpl newsServiceImpl;

    @Before
    public void init() {
        MockitoAnnotations.initMocks(this);
    }

    @Test
    public void testSaveNews() throws ServiceException {
        NewsTO nto = new NewsTO();
        News news = new News();
        news.setId(1L);
        ArrayList<Tag> tags = new ArrayList<>();
        Tag tag = new Tag();
        tag.setId(1L);
        Author author = new Author();
        author.setId(1L);
        nto.setNews(news);
        nto.setAuthor(author);
        nto.setTags(tags);
        try {
            Mockito.when(newsDao.save(news)).thenReturn(1L);
            newsServiceImpl.saveNews(nto);
            Mockito.verify(newsDao, Mockito.atLeastOnce()).save(news);
            Mockito.verify(newsDao, Mockito.times(1)).saveLinkToTags(tags.stream().map(Tag::getId).collect(Collectors.toList()), 1L);
            Mockito.verify(newsDao, Mockito.times(1)).saveLinkToAuthor(author.getId(), 1L);
        } catch (DaoException e) {
            throw new ServiceException(e);
        }
    }

    @Test
    public void testEditNews() throws ServiceException {
        News news = new News();
        news.setId(1L);
        newsServiceImpl.editNews(news);
        try {
            Mockito.verify(newsDao, Mockito.atLeastOnce()).edit(news);
        } catch (DaoException e) {
            throw new ServiceException(e);
        }
    }
    @Test
    public void deleteNews() throws ServiceException {
        newsServiceImpl.deleteNews(1L);
        try {
            Mockito.verify(newsDao, Mockito.atLeastOnce()).delete(1L);
            Mockito.verify(newsDao, Mockito.atLeastOnce()).deleteLinkToAuthor(1L);
            Mockito.verify(newsDao, Mockito.atLeastOnce()).deleteLinkToTags(1L);
        } catch (DaoException e) {
            throw new ServiceException(e);
        }
    }

    @Test
    public void testCountNews() throws ServiceException {
        newsServiceImpl.countNews();
        try {
            Mockito.verify(newsDao, Mockito.atLeastOnce()).countNews();
        } catch (DaoException e) {
            throw new ServiceException(e);
        }
    }

    @Test
    public void testLoadNews() throws ServiceException {
        newsServiceImpl.loadNews(1L);
        try {
            Mockito.verify(newsDao, Mockito.atLeastOnce()).load(1L);
        } catch (DaoException e) {
            throw new ServiceException(e);
        }
    }
    @Test
    public void testLoadAllNews() throws ServiceException {
        newsServiceImpl.loadAllNews();
        try {
            Mockito.verify(newsDao, Mockito.atLeastOnce()).loadAllNews();
        } catch (DaoException e) {
            throw new ServiceException(e);
        }
    }
    @Test
    public void testFindNews() throws ServiceException {
        SearchCriteria criteria = new SearchCriteria();
        newsServiceImpl.findNews(criteria);
        try {
            Mockito.verify(newsDao, Mockito.atLeastOnce()).findNews(criteria);
        } catch (DaoException e) {
            throw new ServiceException(e);
        }
    }


}
