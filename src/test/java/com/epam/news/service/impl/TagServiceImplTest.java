package com.epam.news.service.impl;

import com.epam.news.dao.TagDao;
import com.epam.news.domain.Tag;
import com.epam.news.exception.DaoException;
import com.epam.news.exception.ServiceException;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = "classpath:/context.xml")
public class TagServiceImplTest {
    @Mock
    private TagDao tagDao;
    @InjectMocks
    TagServiceImpl tagService;

    @Before
    public void init() {
        MockitoAnnotations.initMocks(this);
    }

    @Test
    public void testSave() throws ServiceException {
        Tag tag = new Tag(1, "tag");
        tagService.save(tag);
        try {
            Mockito.verify(tagDao, Mockito.times(1)).save(tag);
            Mockito.verify(tagDao, Mockito.times(1)).loadByName(tag.getName());
        } catch (DaoException e) {
            throw new ServiceException(e);
        }
    }

    @Test
    public void testLoad() throws ServiceException {
        tagService.load(1L);
        try {
            Mockito.verify(tagDao, Mockito.times(1)).load(1L);
        } catch (DaoException e) {
            throw new ServiceException(e);
        }
    }

}
