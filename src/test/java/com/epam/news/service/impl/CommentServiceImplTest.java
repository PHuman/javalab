package com.epam.news.service.impl;

import com.epam.news.dao.CommentDao;
import com.epam.news.domain.Comment;
import com.epam.news.exception.DaoException;
import com.epam.news.exception.ServiceException;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import java.sql.Timestamp;
import java.time.LocalDateTime;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = "classpath:/context.xml")
public class CommentServiceImplTest {
    @Mock
    private CommentDao commentDao;
    @InjectMocks
    CommentServiceImpl commentService;

    @Before
    public void init() {
        MockitoAnnotations.initMocks(this);
    }

    @Test
    public void testSave() throws ServiceException {
        Comment comment = new Comment(1, "comment", Timestamp.valueOf(LocalDateTime.now()));
        commentService.save(comment);
        try {
            Mockito.verify(commentDao, Mockito.times(1)).save(comment);
        } catch (DaoException e) {
            throw new ServiceException(e);
        }
    }

    @Test
    public void testDelete() throws ServiceException {
        commentService.delete(1);
        try {
            Mockito.verify(commentDao, Mockito.times(1)).deleteById(1L);
        } catch (DaoException e) {
            throw new ServiceException(e);
        }
    }

    @Test
    public void testDeleteByNewsId() throws ServiceException {
        commentService.deleteByNewsId(1L);
        try {
            Mockito.verify(commentDao, Mockito.times(1)).deleteByNewsId(1L);
        } catch (DaoException e) {
            throw new ServiceException(e);
        }
    }

    @Test
    public void testLoadByNewsId() throws ServiceException {
        commentService.loadByNewsId(1L);
        try {
            Mockito.verify(commentDao, Mockito.times(1)).load(1L);
        } catch (DaoException e) {
            throw new ServiceException(e);
        }
    }


}
