package com.epam.news.service.impl;


import com.epam.news.dao.AuthorDao;
import com.epam.news.domain.Author;
import com.epam.news.exception.DaoException;
import com.epam.news.exception.ServiceException;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = "classpath:/context.xml")
public class AuthorServiceImplTest {
    @Mock
    private AuthorDao authorDao;
    @InjectMocks
    private AuthorServiceImpl authorService;

    @Before
    public void init() {
        MockitoAnnotations.initMocks(this);
    }

    @Test
    public void testSave() throws ServiceException {
        Author author = new Author(1, "Author");
        authorService.save(author);
        try {
            Mockito.verify(authorDao, Mockito.times(1)).save(author);
            Mockito.verify(authorDao, Mockito.times(1)).loadByName(author.getName());
        } catch (DaoException e) {
            throw new ServiceException(e);
        }
    }

    @Test
    public void testLoadByNews() throws ServiceException {
        authorService.loadByNews(1L);
        try {
            Mockito.verify(authorDao, Mockito.times(1)).loadByNews(1L);
        } catch (DaoException e) {
            throw new ServiceException(e);
        }
    }


}
