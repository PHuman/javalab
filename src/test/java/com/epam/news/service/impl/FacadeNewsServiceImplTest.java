package com.epam.news.service.impl;

import com.epam.news.domain.News;
import com.epam.news.exception.ServiceException;
import com.epam.news.service.AuthorService;
import com.epam.news.service.CommentService;
import com.epam.news.service.NewsService;
import com.epam.news.service.TagService;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import java.util.ArrayList;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = "classpath:/context.xml")
public class FacadeNewsServiceImplTest {

    @Mock
    private NewsService newsService;
    @Mock
    private CommentService commentService;
    @Mock
    private AuthorService authorService;
    @Mock
    private TagService tagService;
    @InjectMocks
    private FacadeNewsServiceImpl facadeNewsService;

    @Before
    public void init() {
        MockitoAnnotations.initMocks(this);
    }

    @Test
    public void testDeleteNews() throws ServiceException {
        facadeNewsService.deleteNews(1);
        Mockito.verify(commentService, Mockito.times(1)).deleteByNewsId(1L);
        Mockito.verify(newsService, Mockito.times(1)).deleteNews(1L);
    }

    @Test
    public void testLoadAllNews() throws ServiceException {
        ArrayList<News> news = new ArrayList<>();
        News news1 = new News();
        news1.setId(1L);
        news.add(news1);
        Mockito.when(newsService.loadAllNews()).thenReturn(news);
        facadeNewsService.loadAllNews();
        Mockito.verify(authorService, Mockito.times(1)).loadByNews(1L);
        Mockito.verify(tagService, Mockito.times(1)).load(1L);
        Mockito.verify(commentService, Mockito.times(1)).loadByNewsId(1L);

    }
}

